package goindigo;

import org.testng.annotations.Test;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.AfterTest;
import java.io.File;

/**
 * @author Manjunatha
 *
 */

public class Automation {
// global variable declaration
	public String baseUrl = "https://www.goindigo.in/";
// please change the driver path here
	String driverPath = "F://manju/software/chromedriver.exe";
	public WebDriver driver;

	/*
	 * setting up the chromedriver and properties
	 */
	@BeforeTest
	public void beforeTest() {
		System.out.println("launching website browser");
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver = new ChromeDriver();
		driver.get(baseUrl);
		driver.manage().window().maximize();

	}

	/*
	 * Home page flight seach function first test function( priority =0)
	 */
	@Test(priority = 0)
	public void searchFlight() {
		try {
			Thread.sleep(500);
			driver.findElement(By.name("or-src")).click();
			driver.findElement(By.name("or-src")).clear();
			driver.findElement(By.name("or-src")).sendKeys("blr");
			driver.findElement(By.cssSelector("div.airport-city")).click();
			Thread.sleep(10);
			driver.findElement(By.name("or-dest")).clear();
			driver.findElement(By.name("or-dest")).sendKeys("del");
			driver.findElement(By.xpath("//div[@id='bookFlightTab']/form/div[3]/div/div[2]/div/div/div/div/div/div"))
					.click();
			driver.findElement(By.name("or-depart")).click();
			driver.findElement(By.linkText("15")).click();
			driver.findElement(By.name("passenger")).click();
			driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
			JavascriptExecutor jse = (JavascriptExecutor) driver; // scoll the webpage
			jse.executeScript("scroll(0, 250)");
			driver.findElement(By.cssSelector("button[class='btn btn-primary pax-done']")).click();
			driver.findElement(By.className("hp-src-btn")).click();
			getScreenshot("E:\\flightSearch.png");
		} catch (Exception ex) {
			System.out.println("Exception while searching flight" + ex.getMessage());
		}

	}

	/*
	 * Find the lowest price of flights
	 * 
	 */
	@Test(priority = 1)
	public void findLowestPrice() {
		try {
			Thread.sleep(10000); // wait for loading the page
			driver.findElement(By.xpath("//button[text()='Price']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//button[text()='Price']")).click();
			driver.findElement(By.id("continue-button")).click();
			driver.findElement(By.xpath("//button[text()='Proceed']")).click();
			getScreenshot("E:\\findPrice.png");
		} catch (Exception ex) {
			System.out.println("Exception while findLowestPrice" + ex.getMessage());

		}
	}

	/*
	 * Enter the passenger details
	 */
	@Test(priority = 2)
	public void enterPassengerDetails() {
		try {
			driver.findElement(By.name("mobileNum")).sendKeys("9164731932");
			driver.findElement(By.name("emailId")).sendKeys("manjubit91@gmail.com");
			driver.findElement(By.xpath("//button[text()='Next']")).click();
			Thread.sleep(15000);
			driver.findElement(By.id("pax-title-00")).click();
			driver.findElement(By.xpath("//input[@placeholder='First Name']")).sendKeys("Manjunatha");
			driver.findElement(By.xpath("//input[@placeholder='Last Name']")).sendKeys("Shanbhogue");
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("scroll(0, 250)");
			driver.findElement(By.xpath(
					"/html/body/div[3]/div[3]/div[5]/div/div[2]/div/div[1]/div[2]/div/div/div/div[1]/div/form/div[5]/button[text()='Next']"))
					.click();
			Thread.sleep(1000);
			driver.findElement(By.id("pax-title-01")).click();
			driver.findElement(By.xpath(
					"/html/body/div[3]/div[3]/div[5]/div/div[2]/div/div[1]/div[2]/div/div/div/div[2]/div/form/div[2]/div/input[@placeholder='First Name']"))
					.sendKeys("Narayan");
			driver.findElement(By.xpath(
					"/html/body/div[3]/div[3]/div[5]/div/div[2]/div/div[1]/div[2]/div/div/div/div[2]/div/form/div[3]/div/input[@placeholder='Last Name']"))
					.sendKeys("test");
			driver.findElement(By.xpath(
					"/html/body/div[3]/div[3]/div[5]/div/div[2]/div/div[1]/div[2]/div/div/div/div[2]/div/form/div[5]/button[text()='Next']"))
					.click();
			getScreenshot("E:\\passengerdetails.png");
		}

		catch (Exception ex) {
			System.out.println("Exception while entering PassengerDetail" + ex.getMessage());

		}

	}

	/*
	 * take the screenshot and save in the specfied local drive param : name with
	 * path output: image saved in specified path
	 */
	public void getScreenshot(String screenshotName) throws Exception {
		try {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(screenshotName));
		} catch (Exception ex) {
			System.out.println("Exception while taking screenshot of each test method" + ex.getMessage());
		}
	}

	/*
	 * closing the chrome driver
	 */
	@AfterTest
	public void afterTest() {
		driver.close();

	}

}
