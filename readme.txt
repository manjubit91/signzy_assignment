Open Eclipse ide.
Click on the Help option within the menu.
Select �Eclipse Marketplace..� option within the dropdown.
Install the TestNG for Eclipse plugin.

Click on the File option within the menu -> Click on New -> Select Java Project.
Type "ProductDemo" as the Project Name then click Next.

Now start to import the TestNG Libraries onto our project. Click on the "Libraries" tab, and then "Add Library�" 
On the Add Library dialog, choose "TestNG" and click Next. and click finish

We will now add the JAR files that contain the Selenium API.
These files are found in the Java client driver that can be downloaded from http://docs.seleniumhq.org/download/ 
navigate to where you have placed the Selenium JAR files. 

Now that we are done setting up our project, let us create a new TestNG file. 
Right-click on the "src" package folder then choose New > Other� 
Click on the TestNG folder and select the "TestNG class" option. Click Next. 


ChromeDriver:
Chrome driver can be downloaded from http://docs.seleniumhq.org/download/ -->Accordingly change the path of driver in code. String driverPath = "YOURPATH/chromedriver.exe";

Run the Project:
Right click on the Project and select Run as -> Java application

Test case Report:
Go to test-output ->index.html